import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class UpdateData extends StatefulWidget {
  final int id;
  final String kodebarang;
  final String namabarang;

  UpdateData(
      {required this.id, required this.kodebarang, required this.namabarang});

  @override
  _UpdateDataState createState() => _UpdateDataState();
}

class _UpdateDataState extends State<UpdateData> {
  late TextEditingController controllerKode;
  late TextEditingController controllerNama;

  @override
  void initState() {
    super.initState();
    controllerKode = TextEditingController(text: widget.kodebarang);
    controllerNama = TextEditingController(text: widget.namabarang);
  }

  void dialog(String msg) {
    AlertDialog alertDialog = AlertDialog(
      content: Text(
        "Poses Data " + msg,
        style: TextStyle(color: Colors.blue),
      ),
      actions: [
        ElevatedButton(
          child: Text("OK"),
          onPressed: () {
            Navigator.pop(context);
          },
        )
      ],
    );
    showDialog(builder: (context) => alertDialog, context: context);
  }

  void deleteDialog(String id) {
    AlertDialog alertDialog = AlertDialog(
      content: const Text(
        "Apakah Anda Ingin Menghapus Data",
        style: TextStyle(color: Colors.blue),
      ),
      actions: [
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.red,
          ),
          child: Text("Hapus"),
          onPressed: () {
            deleteData(id);
            Navigator.pop(context);
          },
        ),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.blue,
          ),
          child: Text("Batal"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ],
    );
    showDialog(builder: (context) => alertDialog, context: context);
  }

  void deleteData(String id) async {
    var url = "http://10.0.2.2:1234/delete/";
    var response = await http.delete(
      Uri.parse(url + id),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      var message = data["message"];
      print(message);
      dialog(message);
    }
  }

  void EditData() async {
    var url = "http://10.0.2.2:1234/update";
    var response = await http.put(Uri.parse(url), body: {
      "kodebarang": controllerKode.text,
      "namabarang": controllerNama.text,
      "id": widget.id.toString(),
    });

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      var message = data["message"];
      print(message);
      dialog(message);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Update Data"),
        backgroundColor: Colors.amber,
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text("Kode Barang"),
            const SizedBox(
              height: 10,
            ),
            TextFormField(
              controller: controllerKode,
              decoration: InputDecoration(
                  labelText: "Kode Barang",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10))),
            ),
            const SizedBox(
              height: 10,
            ),
            const Text("Nama Barang"),
            const SizedBox(
              height: 10,
            ),
            TextFormField(
              controller: controllerNama,
              decoration: InputDecoration(
                  labelText: "Nama Barang",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10))),
            ),
            const SizedBox(
              height: 10,
            ),
            ButtonTheme(
                height: 60.0,
                minWidth: double.infinity,
                child: ElevatedButton(
                    onPressed: () {
                      EditData();
                    },
                    child: const Text(
                      "Update Data",
                      style: TextStyle(color: Colors.white),
                    ))),
            ButtonTheme(
                height: 60,
                minWidth: 300,
                child: ElevatedButton(
                    onPressed: () {
                      deleteDialog(widget.id.toString());
                    },
                    child: const Text(
                      "Delete Data",
                      style: TextStyle(color: Colors.white),
                    ))),
          ],
        ),
      ),
    );
  }
}
