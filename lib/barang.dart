class Barang {
  int id;
  String kodebarang;
  String namabarang;

  Barang({ required this.id, required this.kodebarang, required this.namabarang});

  factory Barang.fromJson(Map json) => Barang(
        id: json["id"],
        kodebarang: json["kodebarang"],
        namabarang: json["namabarang"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "kodebarang": kodebarang,
        "namabarang": namabarang,
      };
}
