import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'adddata.dart';
import 'barang.dart';
import 'updatedata.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: "GET DATA FROM GOLANG",
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var loading = false;
  List<Barang> _listProduct = [];

  Future<Null> __fetchDataProduct() async {
    String url = "http://10.0.2.2:1234/barang";
    final response = await get(Uri.parse(url));

    if (response.statusCode == 200) {
      loading = true;
      final data = jsonDecode(response.body);
      print(data);

      setState(() {
        for (Map i in data['data']) {
          _listProduct.add(Barang.fromJson(i));
        }
        loading = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    __fetchDataProduct();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Flutter Get Data"),
        backgroundColor: Colors.amber,
      ),
      body: Container(
        child: loading
            ? const Center(child: CircularProgressIndicator())
            : ListView.builder(
                itemCount: _listProduct.length,
                itemBuilder: (context, i) {
                  final getdata = _listProduct[i];
                  return GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => UpdateData(
                                id: getdata.id,
                                kodebarang: getdata.kodebarang,
                                namabarang: getdata.namabarang,
                              )));
                    },
                    child: Container(
                      child: ItemList(
                          kodebarang: getdata.kodebarang,
                          namabarang: getdata.namabarang),
                    ),
                  );
                }),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => AddPage()));
        },
        label: const Text("Tambah Data"),
        icon: const Icon(Icons.thumb_up),
        backgroundColor: Colors.amber,
      ),
    );
  }
}

class ItemList extends StatefulWidget {
  final String kodebarang;
  final String namabarang;

  ItemList({required this.kodebarang, required this.namabarang});
  @override
  State<ItemList> createState() => _ItemListState();
}

class _ItemListState extends State<ItemList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Card(
            child: ListTile(
              title: Text(widget.namabarang),
              subtitle: Text(widget.kodebarang),
            ),
          )
        ],
      ),
    );
  }
}
