import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class AddPage extends StatefulWidget {
  const AddPage({super.key});

  @override
  State<AddPage> createState() => _AddPageState();
}

class _AddPageState extends State<AddPage> {
  TextEditingController controllerKode = new TextEditingController();
  TextEditingController controllerNama = new TextEditingController();

  void dialog(String msg) {
    AlertDialog alertDialog = AlertDialog(
      content: Text(
        "Poses Data " + msg,
        style: TextStyle(color: Colors.blue),
      ),
      actions: [
        ElevatedButton(
          child: const Text("OK"),
          onPressed: () {
            Navigator.pop(context);
          },
        )
      ],
    );
    showDialog(builder: (context) => alertDialog, context: context);
  }

  void addData() async {
    var url = "http://10.0.2.2:1234/create";
    var response = await http.post(Uri.parse(url), body: {
      "kodebarang": controllerKode.text,
      "namabarang": controllerNama.text,
    });

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      var message = data["message"];
      print(message);
      dialog(message);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Tambah Data"),
        backgroundColor: Colors.amber,
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text("Kode Barang"),
            const SizedBox(
              height: 10,
            ),
            TextFormField(
              controller: controllerKode,
              decoration: InputDecoration(
                  labelText: "Kode Barang",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10))),
            ),
            const SizedBox(
              height: 10,
            ),
            const Text("Nama Barang"),
            const SizedBox(
              height: 10,
            ),
            TextFormField(
              controller: controllerNama,
              decoration: InputDecoration(
                  labelText: "Nama Barang",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10))),
            ),
            const SizedBox(
              height: 10,
            ),
            ButtonTheme(
                height: 60.0,
                minWidth: double.infinity,
                child: ElevatedButton(
                    onPressed: () {
                      addData();
                    },
                    child: const Text(
                      "Simpan Data",
                      style: TextStyle(color: Colors.white),
                    )))
          ],
        ),
      ),
    );
  }
}
